
#include <avr/wdt.h>
#include <SoftwareSerial.h>

enum {
	EOK = 0,
	EINVAL = -1,
	ECOMM = -2,
	EBADMSG = -3,
	ETIMEOUT = -4,
	EMALF = -5,
	ENOPARAM = -6,
	ENOPERM = -7,
	ESPACE = -8,
};

void soft_reset()
{
	asm volatile (" jmp 0");
}

#define hard_reset()      \
do {                        \
	wdt_enable(WDTO_15MS);  \
	for(;;) {}              \
} while(0)

void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
void wdt_init(void)
{
    MCUSR = 0;
    wdt_disable();

    return;
}

#include "common.include"
#include "Tracker.include"

#define SMS_START "I'm alive!"

void setup()
{
	Serial.begin(9600);
	Tracker::init();
	//(void) gsm.send_SMS(SMS_START, sizeof(SMS_START));
	//pinMode(led, OUTPUT);
	//gsm.InitParam(PARAM_SET_1);//configure the module
	//gsm.Echo(1);               //enable AT echo
}

void loop()
{
	Tracker::step();
}

