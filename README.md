
Experimental Arduino-based GSM/GPS tracker
==========================================

Experimental, unfit for use. TBD.


Design
------

* turn on with the ignition
* fuel pump connected to *normally closed*, so it doesn't stop normal car operation when faulty



resources:
-http://www.elecfreaks.com/wiki/index.php?title=EFCom_GPRS/GSM_Shield
-http://www.elecfreaks.com/store/download/product/EFcom/Quick_Start_Guide_EFcom.pdf
-http://www.elecfreaks.com/store/download/product/EFcom/EFcom_Datasheet.pdf
-http://www.instructables.com/id/Tutorial-EFCom-GRPSGSM-Shield-Arduino/?ALLSTEPS
